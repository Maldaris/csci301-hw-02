1. /usr/local/csci301sp2016/homework/HW-CPU-Intro/process-run.py -l 5:100,5:100 -c
Time     PID: 0     PID: 1        CPU        IOs
  1     RUN:cpu      READY          1
  2     RUN:cpu      READY          1
  3     RUN:cpu      READY          1
  4     RUN:cpu      READY          1
  5     RUN:cpu      READY          1
  6        DONE    RUN:cpu          1
  7        DONE    RUN:cpu          1
  8        DONE    RUN:cpu          1
  9        DONE    RUN:cpu          1
 10        DONE    RUN:cpu          1

The CPU Utilization should be 100%, as for each CPU the parameters specify 5 cycles at 100% chance for CPU use.

2. 9 cycles. 4 for each of process, plus 1 for the IO execution call.

3. The CPU cycles should execute while the IO poll waits, as the process can sleep til an IO Interrupt is recieved by the CPU from the IO device.

4. Time     PID: 0     PID: 1        CPU        IOs
  1      RUN:io      READY          1
  2     WAITING      READY                     1
  3     WAITING      READY                     1
  4     WAITING      READY                     1
  5     WAITING      READY                     1
  6*       DONE    RUN:cpu          1
  7        DONE    RUN:cpu          1
  8        DONE    RUN:cpu          1
  9        DONE    RUN:cpu          1

The execution is the same as #2, because process 1 doesn't release it's execution priority.

5. The execution is the same as #3 because the i/o process is forced to switch by virtue of making an IO call.

6. No. The process should execute IO between each process.

7. Because it's likely that the IO process is going to execute another IO call.

8. The CPU is only likely to be <40% active, due to the 3 cycle wait for IO returns. As each process is just as likely to make an IO call, the IO device is likely to be >40% busy.



