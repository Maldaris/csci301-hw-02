
_spork:     file format elf32-i386


Disassembly of section .text:

00000000 <waitForInput>:
#include "types.h"
#include "user.h"
#include "stat.h"


int waitForInput(char* i){
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	81 ec 28 02 00 00    	sub    $0x228,%esp
  char ebuf[512];
  char *e = gets(ebuf, 10);
   9:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
  10:	00 
  11:	8d 85 f4 fd ff ff    	lea    -0x20c(%ebp),%eax
  17:	89 04 24             	mov    %eax,(%esp)
  1a:	e8 a2 01 00 00       	call   1c1 <gets>
  1f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(strcmp(e, i) == 0){
  22:	8b 45 08             	mov    0x8(%ebp),%eax
  25:	89 44 24 04          	mov    %eax,0x4(%esp)
  29:	8b 45 f4             	mov    -0xc(%ebp),%eax
  2c:	89 04 24             	mov    %eax,(%esp)
  2f:	e8 d0 00 00 00       	call   104 <strcmp>
  34:	85 c0                	test   %eax,%eax
  36:	75 07                	jne    3f <waitForInput+0x3f>
    return 1;
  38:	b8 01 00 00 00       	mov    $0x1,%eax
  3d:	eb 0b                	jmp    4a <waitForInput+0x4a>
  } else {
    return waitForInput(i);
  3f:	8b 45 08             	mov    0x8(%ebp),%eax
  42:	89 04 24             	mov    %eax,(%esp)
  45:	e8 b6 ff ff ff       	call   0 <waitForInput>
  }
}
  4a:	c9                   	leave  
  4b:	c3                   	ret    

0000004c <main>:

int main(int argc, char** argv){
  4c:	55                   	push   %ebp
  4d:	89 e5                	mov    %esp,%ebp
  4f:	83 e4 f0             	and    $0xfffffff0,%esp
  52:	83 ec 20             	sub    $0x20,%esp
  while(1){
    waitForInput("go\n");
  55:	c7 04 24 53 08 00 00 	movl   $0x853,(%esp)
  5c:	e8 9f ff ff ff       	call   0 <waitForInput>
    int pid = fork();
  61:	e8 a2 02 00 00       	call   308 <fork>
  66:	89 44 24 1c          	mov    %eax,0x1c(%esp)
    if(pid == 0){
  6a:	83 7c 24 1c 00       	cmpl   $0x0,0x1c(%esp)
  6f:	75 16                	jne    87 <main+0x3b>
      char *nargs[] = {};
      exec("itsaspork", nargs);
  71:	8d 44 24 1c          	lea    0x1c(%esp),%eax
  75:	89 44 24 04          	mov    %eax,0x4(%esp)
  79:	c7 04 24 57 08 00 00 	movl   $0x857,(%esp)
  80:	e8 c3 02 00 00       	call   348 <exec>
      kill(pid);
      wait();
      exit();
      return 0;
    }
  }
  85:	eb ce                	jmp    55 <main+0x9>
    int pid = fork();
    if(pid == 0){
      char *nargs[] = {};
      exec("itsaspork", nargs);
    } else {
      waitForInput("stop\n");
  87:	c7 04 24 61 08 00 00 	movl   $0x861,(%esp)
  8e:	e8 6d ff ff ff       	call   0 <waitForInput>
      kill(pid);
  93:	8b 44 24 1c          	mov    0x1c(%esp),%eax
  97:	89 04 24             	mov    %eax,(%esp)
  9a:	e8 a1 02 00 00       	call   340 <kill>
      wait();
  9f:	e8 74 02 00 00       	call   318 <wait>
      exit();
  a4:	e8 67 02 00 00       	call   310 <exit>
  a9:	90                   	nop
  aa:	90                   	nop
  ab:	90                   	nop

000000ac <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  ac:	55                   	push   %ebp
  ad:	89 e5                	mov    %esp,%ebp
  af:	57                   	push   %edi
  b0:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  b1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  b4:	8b 55 10             	mov    0x10(%ebp),%edx
  b7:	8b 45 0c             	mov    0xc(%ebp),%eax
  ba:	89 cb                	mov    %ecx,%ebx
  bc:	89 df                	mov    %ebx,%edi
  be:	89 d1                	mov    %edx,%ecx
  c0:	fc                   	cld    
  c1:	f3 aa                	rep stos %al,%es:(%edi)
  c3:	89 ca                	mov    %ecx,%edx
  c5:	89 fb                	mov    %edi,%ebx
  c7:	89 5d 08             	mov    %ebx,0x8(%ebp)
  ca:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  cd:	5b                   	pop    %ebx
  ce:	5f                   	pop    %edi
  cf:	5d                   	pop    %ebp
  d0:	c3                   	ret    

000000d1 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  d1:	55                   	push   %ebp
  d2:	89 e5                	mov    %esp,%ebp
  d4:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  d7:	8b 45 08             	mov    0x8(%ebp),%eax
  da:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  dd:	8b 45 0c             	mov    0xc(%ebp),%eax
  e0:	0f b6 10             	movzbl (%eax),%edx
  e3:	8b 45 08             	mov    0x8(%ebp),%eax
  e6:	88 10                	mov    %dl,(%eax)
  e8:	8b 45 08             	mov    0x8(%ebp),%eax
  eb:	0f b6 00             	movzbl (%eax),%eax
  ee:	84 c0                	test   %al,%al
  f0:	0f 95 c0             	setne  %al
  f3:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  f7:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  fb:	84 c0                	test   %al,%al
  fd:	75 de                	jne    dd <strcpy+0xc>
    ;
  return os;
  ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 102:	c9                   	leave  
 103:	c3                   	ret    

00000104 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 104:	55                   	push   %ebp
 105:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 107:	eb 08                	jmp    111 <strcmp+0xd>
    p++, q++;
 109:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 10d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 111:	8b 45 08             	mov    0x8(%ebp),%eax
 114:	0f b6 00             	movzbl (%eax),%eax
 117:	84 c0                	test   %al,%al
 119:	74 10                	je     12b <strcmp+0x27>
 11b:	8b 45 08             	mov    0x8(%ebp),%eax
 11e:	0f b6 10             	movzbl (%eax),%edx
 121:	8b 45 0c             	mov    0xc(%ebp),%eax
 124:	0f b6 00             	movzbl (%eax),%eax
 127:	38 c2                	cmp    %al,%dl
 129:	74 de                	je     109 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 12b:	8b 45 08             	mov    0x8(%ebp),%eax
 12e:	0f b6 00             	movzbl (%eax),%eax
 131:	0f b6 d0             	movzbl %al,%edx
 134:	8b 45 0c             	mov    0xc(%ebp),%eax
 137:	0f b6 00             	movzbl (%eax),%eax
 13a:	0f b6 c0             	movzbl %al,%eax
 13d:	89 d1                	mov    %edx,%ecx
 13f:	29 c1                	sub    %eax,%ecx
 141:	89 c8                	mov    %ecx,%eax
}
 143:	5d                   	pop    %ebp
 144:	c3                   	ret    

00000145 <strlen>:

uint
strlen(char *s)
{
 145:	55                   	push   %ebp
 146:	89 e5                	mov    %esp,%ebp
 148:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 14b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 152:	eb 04                	jmp    158 <strlen+0x13>
 154:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 158:	8b 45 fc             	mov    -0x4(%ebp),%eax
 15b:	03 45 08             	add    0x8(%ebp),%eax
 15e:	0f b6 00             	movzbl (%eax),%eax
 161:	84 c0                	test   %al,%al
 163:	75 ef                	jne    154 <strlen+0xf>
    ;
  return n;
 165:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 168:	c9                   	leave  
 169:	c3                   	ret    

0000016a <memset>:

void*
memset(void *dst, int c, uint n)
{
 16a:	55                   	push   %ebp
 16b:	89 e5                	mov    %esp,%ebp
 16d:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 170:	8b 45 10             	mov    0x10(%ebp),%eax
 173:	89 44 24 08          	mov    %eax,0x8(%esp)
 177:	8b 45 0c             	mov    0xc(%ebp),%eax
 17a:	89 44 24 04          	mov    %eax,0x4(%esp)
 17e:	8b 45 08             	mov    0x8(%ebp),%eax
 181:	89 04 24             	mov    %eax,(%esp)
 184:	e8 23 ff ff ff       	call   ac <stosb>
  return dst;
 189:	8b 45 08             	mov    0x8(%ebp),%eax
}
 18c:	c9                   	leave  
 18d:	c3                   	ret    

0000018e <strchr>:

char*
strchr(const char *s, char c)
{
 18e:	55                   	push   %ebp
 18f:	89 e5                	mov    %esp,%ebp
 191:	83 ec 04             	sub    $0x4,%esp
 194:	8b 45 0c             	mov    0xc(%ebp),%eax
 197:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 19a:	eb 14                	jmp    1b0 <strchr+0x22>
    if(*s == c)
 19c:	8b 45 08             	mov    0x8(%ebp),%eax
 19f:	0f b6 00             	movzbl (%eax),%eax
 1a2:	3a 45 fc             	cmp    -0x4(%ebp),%al
 1a5:	75 05                	jne    1ac <strchr+0x1e>
      return (char*)s;
 1a7:	8b 45 08             	mov    0x8(%ebp),%eax
 1aa:	eb 13                	jmp    1bf <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 1ac:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1b0:	8b 45 08             	mov    0x8(%ebp),%eax
 1b3:	0f b6 00             	movzbl (%eax),%eax
 1b6:	84 c0                	test   %al,%al
 1b8:	75 e2                	jne    19c <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 1ba:	b8 00 00 00 00       	mov    $0x0,%eax
}
 1bf:	c9                   	leave  
 1c0:	c3                   	ret    

000001c1 <gets>:

char*
gets(char *buf, int max)
{
 1c1:	55                   	push   %ebp
 1c2:	89 e5                	mov    %esp,%ebp
 1c4:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1c7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1ce:	eb 44                	jmp    214 <gets+0x53>
    cc = read(0, &c, 1);
 1d0:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 1d7:	00 
 1d8:	8d 45 ef             	lea    -0x11(%ebp),%eax
 1db:	89 44 24 04          	mov    %eax,0x4(%esp)
 1df:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1e6:	e8 3d 01 00 00       	call   328 <read>
 1eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 1ee:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1f2:	7e 2d                	jle    221 <gets+0x60>
      break;
    buf[i++] = c;
 1f4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1f7:	03 45 08             	add    0x8(%ebp),%eax
 1fa:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 1fe:	88 10                	mov    %dl,(%eax)
 200:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 204:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 208:	3c 0a                	cmp    $0xa,%al
 20a:	74 16                	je     222 <gets+0x61>
 20c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 210:	3c 0d                	cmp    $0xd,%al
 212:	74 0e                	je     222 <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 214:	8b 45 f0             	mov    -0x10(%ebp),%eax
 217:	83 c0 01             	add    $0x1,%eax
 21a:	3b 45 0c             	cmp    0xc(%ebp),%eax
 21d:	7c b1                	jl     1d0 <gets+0xf>
 21f:	eb 01                	jmp    222 <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 221:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 222:	8b 45 f0             	mov    -0x10(%ebp),%eax
 225:	03 45 08             	add    0x8(%ebp),%eax
 228:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 22b:	8b 45 08             	mov    0x8(%ebp),%eax
}
 22e:	c9                   	leave  
 22f:	c3                   	ret    

00000230 <stat>:

int
stat(char *n, struct stat *st)
{
 230:	55                   	push   %ebp
 231:	89 e5                	mov    %esp,%ebp
 233:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 236:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 23d:	00 
 23e:	8b 45 08             	mov    0x8(%ebp),%eax
 241:	89 04 24             	mov    %eax,(%esp)
 244:	e8 07 01 00 00       	call   350 <open>
 249:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 24c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 250:	79 07                	jns    259 <stat+0x29>
    return -1;
 252:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 257:	eb 23                	jmp    27c <stat+0x4c>
  r = fstat(fd, st);
 259:	8b 45 0c             	mov    0xc(%ebp),%eax
 25c:	89 44 24 04          	mov    %eax,0x4(%esp)
 260:	8b 45 f0             	mov    -0x10(%ebp),%eax
 263:	89 04 24             	mov    %eax,(%esp)
 266:	e8 fd 00 00 00       	call   368 <fstat>
 26b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 26e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 271:	89 04 24             	mov    %eax,(%esp)
 274:	e8 bf 00 00 00       	call   338 <close>
  return r;
 279:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 27c:	c9                   	leave  
 27d:	c3                   	ret    

0000027e <atoi>:

int
atoi(const char *s)
{
 27e:	55                   	push   %ebp
 27f:	89 e5                	mov    %esp,%ebp
 281:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 284:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 28b:	eb 24                	jmp    2b1 <atoi+0x33>
    n = n*10 + *s++ - '0';
 28d:	8b 55 fc             	mov    -0x4(%ebp),%edx
 290:	89 d0                	mov    %edx,%eax
 292:	c1 e0 02             	shl    $0x2,%eax
 295:	01 d0                	add    %edx,%eax
 297:	01 c0                	add    %eax,%eax
 299:	89 c2                	mov    %eax,%edx
 29b:	8b 45 08             	mov    0x8(%ebp),%eax
 29e:	0f b6 00             	movzbl (%eax),%eax
 2a1:	0f be c0             	movsbl %al,%eax
 2a4:	8d 04 02             	lea    (%edx,%eax,1),%eax
 2a7:	83 e8 30             	sub    $0x30,%eax
 2aa:	89 45 fc             	mov    %eax,-0x4(%ebp)
 2ad:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 2b1:	8b 45 08             	mov    0x8(%ebp),%eax
 2b4:	0f b6 00             	movzbl (%eax),%eax
 2b7:	3c 2f                	cmp    $0x2f,%al
 2b9:	7e 0a                	jle    2c5 <atoi+0x47>
 2bb:	8b 45 08             	mov    0x8(%ebp),%eax
 2be:	0f b6 00             	movzbl (%eax),%eax
 2c1:	3c 39                	cmp    $0x39,%al
 2c3:	7e c8                	jle    28d <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 2c5:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2c8:	c9                   	leave  
 2c9:	c3                   	ret    

000002ca <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 2ca:	55                   	push   %ebp
 2cb:	89 e5                	mov    %esp,%ebp
 2cd:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 2d0:	8b 45 08             	mov    0x8(%ebp),%eax
 2d3:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 2d6:	8b 45 0c             	mov    0xc(%ebp),%eax
 2d9:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 2dc:	eb 13                	jmp    2f1 <memmove+0x27>
    *dst++ = *src++;
 2de:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2e1:	0f b6 10             	movzbl (%eax),%edx
 2e4:	8b 45 f8             	mov    -0x8(%ebp),%eax
 2e7:	88 10                	mov    %dl,(%eax)
 2e9:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 2ed:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2f1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 2f5:	0f 9f c0             	setg   %al
 2f8:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 2fc:	84 c0                	test   %al,%al
 2fe:	75 de                	jne    2de <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 300:	8b 45 08             	mov    0x8(%ebp),%eax
}
 303:	c9                   	leave  
 304:	c3                   	ret    
 305:	90                   	nop
 306:	90                   	nop
 307:	90                   	nop

00000308 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 308:	b8 01 00 00 00       	mov    $0x1,%eax
 30d:	cd 40                	int    $0x40
 30f:	c3                   	ret    

00000310 <exit>:
SYSCALL(exit)
 310:	b8 02 00 00 00       	mov    $0x2,%eax
 315:	cd 40                	int    $0x40
 317:	c3                   	ret    

00000318 <wait>:
SYSCALL(wait)
 318:	b8 03 00 00 00       	mov    $0x3,%eax
 31d:	cd 40                	int    $0x40
 31f:	c3                   	ret    

00000320 <pipe>:
SYSCALL(pipe)
 320:	b8 04 00 00 00       	mov    $0x4,%eax
 325:	cd 40                	int    $0x40
 327:	c3                   	ret    

00000328 <read>:
SYSCALL(read)
 328:	b8 05 00 00 00       	mov    $0x5,%eax
 32d:	cd 40                	int    $0x40
 32f:	c3                   	ret    

00000330 <write>:
SYSCALL(write)
 330:	b8 10 00 00 00       	mov    $0x10,%eax
 335:	cd 40                	int    $0x40
 337:	c3                   	ret    

00000338 <close>:
SYSCALL(close)
 338:	b8 15 00 00 00       	mov    $0x15,%eax
 33d:	cd 40                	int    $0x40
 33f:	c3                   	ret    

00000340 <kill>:
SYSCALL(kill)
 340:	b8 06 00 00 00       	mov    $0x6,%eax
 345:	cd 40                	int    $0x40
 347:	c3                   	ret    

00000348 <exec>:
SYSCALL(exec)
 348:	b8 07 00 00 00       	mov    $0x7,%eax
 34d:	cd 40                	int    $0x40
 34f:	c3                   	ret    

00000350 <open>:
SYSCALL(open)
 350:	b8 0f 00 00 00       	mov    $0xf,%eax
 355:	cd 40                	int    $0x40
 357:	c3                   	ret    

00000358 <mknod>:
SYSCALL(mknod)
 358:	b8 11 00 00 00       	mov    $0x11,%eax
 35d:	cd 40                	int    $0x40
 35f:	c3                   	ret    

00000360 <unlink>:
SYSCALL(unlink)
 360:	b8 12 00 00 00       	mov    $0x12,%eax
 365:	cd 40                	int    $0x40
 367:	c3                   	ret    

00000368 <fstat>:
SYSCALL(fstat)
 368:	b8 08 00 00 00       	mov    $0x8,%eax
 36d:	cd 40                	int    $0x40
 36f:	c3                   	ret    

00000370 <link>:
SYSCALL(link)
 370:	b8 13 00 00 00       	mov    $0x13,%eax
 375:	cd 40                	int    $0x40
 377:	c3                   	ret    

00000378 <mkdir>:
SYSCALL(mkdir)
 378:	b8 14 00 00 00       	mov    $0x14,%eax
 37d:	cd 40                	int    $0x40
 37f:	c3                   	ret    

00000380 <chdir>:
SYSCALL(chdir)
 380:	b8 09 00 00 00       	mov    $0x9,%eax
 385:	cd 40                	int    $0x40
 387:	c3                   	ret    

00000388 <dup>:
SYSCALL(dup)
 388:	b8 0a 00 00 00       	mov    $0xa,%eax
 38d:	cd 40                	int    $0x40
 38f:	c3                   	ret    

00000390 <getpid>:
SYSCALL(getpid)
 390:	b8 0b 00 00 00       	mov    $0xb,%eax
 395:	cd 40                	int    $0x40
 397:	c3                   	ret    

00000398 <sbrk>:
SYSCALL(sbrk)
 398:	b8 0c 00 00 00       	mov    $0xc,%eax
 39d:	cd 40                	int    $0x40
 39f:	c3                   	ret    

000003a0 <sleep>:
SYSCALL(sleep)
 3a0:	b8 0d 00 00 00       	mov    $0xd,%eax
 3a5:	cd 40                	int    $0x40
 3a7:	c3                   	ret    

000003a8 <uptime>:
SYSCALL(uptime)
 3a8:	b8 0e 00 00 00       	mov    $0xe,%eax
 3ad:	cd 40                	int    $0x40
 3af:	c3                   	ret    

000003b0 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 3b0:	55                   	push   %ebp
 3b1:	89 e5                	mov    %esp,%ebp
 3b3:	83 ec 28             	sub    $0x28,%esp
 3b6:	8b 45 0c             	mov    0xc(%ebp),%eax
 3b9:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 3bc:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 3c3:	00 
 3c4:	8d 45 f4             	lea    -0xc(%ebp),%eax
 3c7:	89 44 24 04          	mov    %eax,0x4(%esp)
 3cb:	8b 45 08             	mov    0x8(%ebp),%eax
 3ce:	89 04 24             	mov    %eax,(%esp)
 3d1:	e8 5a ff ff ff       	call   330 <write>
}
 3d6:	c9                   	leave  
 3d7:	c3                   	ret    

000003d8 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 3d8:	55                   	push   %ebp
 3d9:	89 e5                	mov    %esp,%ebp
 3db:	53                   	push   %ebx
 3dc:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 3df:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 3e6:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 3ea:	74 17                	je     403 <printint+0x2b>
 3ec:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 3f0:	79 11                	jns    403 <printint+0x2b>
    neg = 1;
 3f2:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 3f9:	8b 45 0c             	mov    0xc(%ebp),%eax
 3fc:	f7 d8                	neg    %eax
 3fe:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 401:	eb 06                	jmp    409 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 403:	8b 45 0c             	mov    0xc(%ebp),%eax
 406:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 409:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 410:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 413:	8b 5d 10             	mov    0x10(%ebp),%ebx
 416:	8b 45 f4             	mov    -0xc(%ebp),%eax
 419:	ba 00 00 00 00       	mov    $0x0,%edx
 41e:	f7 f3                	div    %ebx
 420:	89 d0                	mov    %edx,%eax
 422:	0f b6 80 70 08 00 00 	movzbl 0x870(%eax),%eax
 429:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 42d:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 431:	8b 45 10             	mov    0x10(%ebp),%eax
 434:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 437:	8b 45 f4             	mov    -0xc(%ebp),%eax
 43a:	ba 00 00 00 00       	mov    $0x0,%edx
 43f:	f7 75 d4             	divl   -0x2c(%ebp)
 442:	89 45 f4             	mov    %eax,-0xc(%ebp)
 445:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 449:	75 c5                	jne    410 <printint+0x38>
  if(neg)
 44b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 44f:	74 2a                	je     47b <printint+0xa3>
    buf[i++] = '-';
 451:	8b 45 ec             	mov    -0x14(%ebp),%eax
 454:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 459:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 45d:	eb 1d                	jmp    47c <printint+0xa4>
    putc(fd, buf[i]);
 45f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 462:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 467:	0f be c0             	movsbl %al,%eax
 46a:	89 44 24 04          	mov    %eax,0x4(%esp)
 46e:	8b 45 08             	mov    0x8(%ebp),%eax
 471:	89 04 24             	mov    %eax,(%esp)
 474:	e8 37 ff ff ff       	call   3b0 <putc>
 479:	eb 01                	jmp    47c <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 47b:	90                   	nop
 47c:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 480:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 484:	79 d9                	jns    45f <printint+0x87>
    putc(fd, buf[i]);
}
 486:	83 c4 44             	add    $0x44,%esp
 489:	5b                   	pop    %ebx
 48a:	5d                   	pop    %ebp
 48b:	c3                   	ret    

0000048c <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 48c:	55                   	push   %ebp
 48d:	89 e5                	mov    %esp,%ebp
 48f:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 492:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 499:	8d 45 0c             	lea    0xc(%ebp),%eax
 49c:	83 c0 04             	add    $0x4,%eax
 49f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 4a2:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 4a9:	e9 7e 01 00 00       	jmp    62c <printf+0x1a0>
    c = fmt[i] & 0xff;
 4ae:	8b 55 0c             	mov    0xc(%ebp),%edx
 4b1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4b4:	8d 04 02             	lea    (%edx,%eax,1),%eax
 4b7:	0f b6 00             	movzbl (%eax),%eax
 4ba:	0f be c0             	movsbl %al,%eax
 4bd:	25 ff 00 00 00       	and    $0xff,%eax
 4c2:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 4c5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4c9:	75 2c                	jne    4f7 <printf+0x6b>
      if(c == '%'){
 4cb:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 4cf:	75 0c                	jne    4dd <printf+0x51>
        state = '%';
 4d1:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 4d8:	e9 4b 01 00 00       	jmp    628 <printf+0x19c>
      } else {
        putc(fd, c);
 4dd:	8b 45 e8             	mov    -0x18(%ebp),%eax
 4e0:	0f be c0             	movsbl %al,%eax
 4e3:	89 44 24 04          	mov    %eax,0x4(%esp)
 4e7:	8b 45 08             	mov    0x8(%ebp),%eax
 4ea:	89 04 24             	mov    %eax,(%esp)
 4ed:	e8 be fe ff ff       	call   3b0 <putc>
 4f2:	e9 31 01 00 00       	jmp    628 <printf+0x19c>
      }
    } else if(state == '%'){
 4f7:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 4fb:	0f 85 27 01 00 00    	jne    628 <printf+0x19c>
      if(c == 'd'){
 501:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 505:	75 2d                	jne    534 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 507:	8b 45 f4             	mov    -0xc(%ebp),%eax
 50a:	8b 00                	mov    (%eax),%eax
 50c:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 513:	00 
 514:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 51b:	00 
 51c:	89 44 24 04          	mov    %eax,0x4(%esp)
 520:	8b 45 08             	mov    0x8(%ebp),%eax
 523:	89 04 24             	mov    %eax,(%esp)
 526:	e8 ad fe ff ff       	call   3d8 <printint>
        ap++;
 52b:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 52f:	e9 ed 00 00 00       	jmp    621 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 534:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 538:	74 06                	je     540 <printf+0xb4>
 53a:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 53e:	75 2d                	jne    56d <printf+0xe1>
        printint(fd, *ap, 16, 0);
 540:	8b 45 f4             	mov    -0xc(%ebp),%eax
 543:	8b 00                	mov    (%eax),%eax
 545:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 54c:	00 
 54d:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 554:	00 
 555:	89 44 24 04          	mov    %eax,0x4(%esp)
 559:	8b 45 08             	mov    0x8(%ebp),%eax
 55c:	89 04 24             	mov    %eax,(%esp)
 55f:	e8 74 fe ff ff       	call   3d8 <printint>
        ap++;
 564:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 568:	e9 b4 00 00 00       	jmp    621 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 56d:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 571:	75 46                	jne    5b9 <printf+0x12d>
        s = (char*)*ap;
 573:	8b 45 f4             	mov    -0xc(%ebp),%eax
 576:	8b 00                	mov    (%eax),%eax
 578:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 57b:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 57f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 583:	75 27                	jne    5ac <printf+0x120>
          s = "(null)";
 585:	c7 45 e4 67 08 00 00 	movl   $0x867,-0x1c(%ebp)
        while(*s != 0){
 58c:	eb 1f                	jmp    5ad <printf+0x121>
          putc(fd, *s);
 58e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 591:	0f b6 00             	movzbl (%eax),%eax
 594:	0f be c0             	movsbl %al,%eax
 597:	89 44 24 04          	mov    %eax,0x4(%esp)
 59b:	8b 45 08             	mov    0x8(%ebp),%eax
 59e:	89 04 24             	mov    %eax,(%esp)
 5a1:	e8 0a fe ff ff       	call   3b0 <putc>
          s++;
 5a6:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 5aa:	eb 01                	jmp    5ad <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 5ac:	90                   	nop
 5ad:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5b0:	0f b6 00             	movzbl (%eax),%eax
 5b3:	84 c0                	test   %al,%al
 5b5:	75 d7                	jne    58e <printf+0x102>
 5b7:	eb 68                	jmp    621 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5b9:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 5bd:	75 1d                	jne    5dc <printf+0x150>
        putc(fd, *ap);
 5bf:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5c2:	8b 00                	mov    (%eax),%eax
 5c4:	0f be c0             	movsbl %al,%eax
 5c7:	89 44 24 04          	mov    %eax,0x4(%esp)
 5cb:	8b 45 08             	mov    0x8(%ebp),%eax
 5ce:	89 04 24             	mov    %eax,(%esp)
 5d1:	e8 da fd ff ff       	call   3b0 <putc>
        ap++;
 5d6:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 5da:	eb 45                	jmp    621 <printf+0x195>
      } else if(c == '%'){
 5dc:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 5e0:	75 17                	jne    5f9 <printf+0x16d>
        putc(fd, c);
 5e2:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5e5:	0f be c0             	movsbl %al,%eax
 5e8:	89 44 24 04          	mov    %eax,0x4(%esp)
 5ec:	8b 45 08             	mov    0x8(%ebp),%eax
 5ef:	89 04 24             	mov    %eax,(%esp)
 5f2:	e8 b9 fd ff ff       	call   3b0 <putc>
 5f7:	eb 28                	jmp    621 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 5f9:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 600:	00 
 601:	8b 45 08             	mov    0x8(%ebp),%eax
 604:	89 04 24             	mov    %eax,(%esp)
 607:	e8 a4 fd ff ff       	call   3b0 <putc>
        putc(fd, c);
 60c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 60f:	0f be c0             	movsbl %al,%eax
 612:	89 44 24 04          	mov    %eax,0x4(%esp)
 616:	8b 45 08             	mov    0x8(%ebp),%eax
 619:	89 04 24             	mov    %eax,(%esp)
 61c:	e8 8f fd ff ff       	call   3b0 <putc>
      }
      state = 0;
 621:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 628:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 62c:	8b 55 0c             	mov    0xc(%ebp),%edx
 62f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 632:	8d 04 02             	lea    (%edx,%eax,1),%eax
 635:	0f b6 00             	movzbl (%eax),%eax
 638:	84 c0                	test   %al,%al
 63a:	0f 85 6e fe ff ff    	jne    4ae <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 640:	c9                   	leave  
 641:	c3                   	ret    
 642:	90                   	nop
 643:	90                   	nop

00000644 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 644:	55                   	push   %ebp
 645:	89 e5                	mov    %esp,%ebp
 647:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 64a:	8b 45 08             	mov    0x8(%ebp),%eax
 64d:	83 e8 08             	sub    $0x8,%eax
 650:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 653:	a1 8c 08 00 00       	mov    0x88c,%eax
 658:	89 45 fc             	mov    %eax,-0x4(%ebp)
 65b:	eb 24                	jmp    681 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 65d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 660:	8b 00                	mov    (%eax),%eax
 662:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 665:	77 12                	ja     679 <free+0x35>
 667:	8b 45 f8             	mov    -0x8(%ebp),%eax
 66a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 66d:	77 24                	ja     693 <free+0x4f>
 66f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 672:	8b 00                	mov    (%eax),%eax
 674:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 677:	77 1a                	ja     693 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 679:	8b 45 fc             	mov    -0x4(%ebp),%eax
 67c:	8b 00                	mov    (%eax),%eax
 67e:	89 45 fc             	mov    %eax,-0x4(%ebp)
 681:	8b 45 f8             	mov    -0x8(%ebp),%eax
 684:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 687:	76 d4                	jbe    65d <free+0x19>
 689:	8b 45 fc             	mov    -0x4(%ebp),%eax
 68c:	8b 00                	mov    (%eax),%eax
 68e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 691:	76 ca                	jbe    65d <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 693:	8b 45 f8             	mov    -0x8(%ebp),%eax
 696:	8b 40 04             	mov    0x4(%eax),%eax
 699:	c1 e0 03             	shl    $0x3,%eax
 69c:	89 c2                	mov    %eax,%edx
 69e:	03 55 f8             	add    -0x8(%ebp),%edx
 6a1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6a4:	8b 00                	mov    (%eax),%eax
 6a6:	39 c2                	cmp    %eax,%edx
 6a8:	75 24                	jne    6ce <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 6aa:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ad:	8b 50 04             	mov    0x4(%eax),%edx
 6b0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6b3:	8b 00                	mov    (%eax),%eax
 6b5:	8b 40 04             	mov    0x4(%eax),%eax
 6b8:	01 c2                	add    %eax,%edx
 6ba:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6bd:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 6c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6c3:	8b 00                	mov    (%eax),%eax
 6c5:	8b 10                	mov    (%eax),%edx
 6c7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ca:	89 10                	mov    %edx,(%eax)
 6cc:	eb 0a                	jmp    6d8 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 6ce:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d1:	8b 10                	mov    (%eax),%edx
 6d3:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6d6:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 6d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6db:	8b 40 04             	mov    0x4(%eax),%eax
 6de:	c1 e0 03             	shl    $0x3,%eax
 6e1:	03 45 fc             	add    -0x4(%ebp),%eax
 6e4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6e7:	75 20                	jne    709 <free+0xc5>
    p->s.size += bp->s.size;
 6e9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ec:	8b 50 04             	mov    0x4(%eax),%edx
 6ef:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6f2:	8b 40 04             	mov    0x4(%eax),%eax
 6f5:	01 c2                	add    %eax,%edx
 6f7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6fa:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 6fd:	8b 45 f8             	mov    -0x8(%ebp),%eax
 700:	8b 10                	mov    (%eax),%edx
 702:	8b 45 fc             	mov    -0x4(%ebp),%eax
 705:	89 10                	mov    %edx,(%eax)
 707:	eb 08                	jmp    711 <free+0xcd>
  } else
    p->s.ptr = bp;
 709:	8b 45 fc             	mov    -0x4(%ebp),%eax
 70c:	8b 55 f8             	mov    -0x8(%ebp),%edx
 70f:	89 10                	mov    %edx,(%eax)
  freep = p;
 711:	8b 45 fc             	mov    -0x4(%ebp),%eax
 714:	a3 8c 08 00 00       	mov    %eax,0x88c
}
 719:	c9                   	leave  
 71a:	c3                   	ret    

0000071b <morecore>:

static Header*
morecore(uint nu)
{
 71b:	55                   	push   %ebp
 71c:	89 e5                	mov    %esp,%ebp
 71e:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 721:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 728:	77 07                	ja     731 <morecore+0x16>
    nu = 4096;
 72a:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 731:	8b 45 08             	mov    0x8(%ebp),%eax
 734:	c1 e0 03             	shl    $0x3,%eax
 737:	89 04 24             	mov    %eax,(%esp)
 73a:	e8 59 fc ff ff       	call   398 <sbrk>
 73f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 742:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 746:	75 07                	jne    74f <morecore+0x34>
    return 0;
 748:	b8 00 00 00 00       	mov    $0x0,%eax
 74d:	eb 22                	jmp    771 <morecore+0x56>
  hp = (Header*)p;
 74f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 752:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 755:	8b 45 f4             	mov    -0xc(%ebp),%eax
 758:	8b 55 08             	mov    0x8(%ebp),%edx
 75b:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 75e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 761:	83 c0 08             	add    $0x8,%eax
 764:	89 04 24             	mov    %eax,(%esp)
 767:	e8 d8 fe ff ff       	call   644 <free>
  return freep;
 76c:	a1 8c 08 00 00       	mov    0x88c,%eax
}
 771:	c9                   	leave  
 772:	c3                   	ret    

00000773 <malloc>:

void*
malloc(uint nbytes)
{
 773:	55                   	push   %ebp
 774:	89 e5                	mov    %esp,%ebp
 776:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 779:	8b 45 08             	mov    0x8(%ebp),%eax
 77c:	83 c0 07             	add    $0x7,%eax
 77f:	c1 e8 03             	shr    $0x3,%eax
 782:	83 c0 01             	add    $0x1,%eax
 785:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 788:	a1 8c 08 00 00       	mov    0x88c,%eax
 78d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 790:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 794:	75 23                	jne    7b9 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 796:	c7 45 f0 84 08 00 00 	movl   $0x884,-0x10(%ebp)
 79d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7a0:	a3 8c 08 00 00       	mov    %eax,0x88c
 7a5:	a1 8c 08 00 00       	mov    0x88c,%eax
 7aa:	a3 84 08 00 00       	mov    %eax,0x884
    base.s.size = 0;
 7af:	c7 05 88 08 00 00 00 	movl   $0x0,0x888
 7b6:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7b9:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7bc:	8b 00                	mov    (%eax),%eax
 7be:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 7c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7c4:	8b 40 04             	mov    0x4(%eax),%eax
 7c7:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7ca:	72 4d                	jb     819 <malloc+0xa6>
      if(p->s.size == nunits)
 7cc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7cf:	8b 40 04             	mov    0x4(%eax),%eax
 7d2:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7d5:	75 0c                	jne    7e3 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 7d7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7da:	8b 10                	mov    (%eax),%edx
 7dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7df:	89 10                	mov    %edx,(%eax)
 7e1:	eb 26                	jmp    809 <malloc+0x96>
      else {
        p->s.size -= nunits;
 7e3:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7e6:	8b 40 04             	mov    0x4(%eax),%eax
 7e9:	89 c2                	mov    %eax,%edx
 7eb:	2b 55 f4             	sub    -0xc(%ebp),%edx
 7ee:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7f1:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 7f4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7f7:	8b 40 04             	mov    0x4(%eax),%eax
 7fa:	c1 e0 03             	shl    $0x3,%eax
 7fd:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 800:	8b 45 ec             	mov    -0x14(%ebp),%eax
 803:	8b 55 f4             	mov    -0xc(%ebp),%edx
 806:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 809:	8b 45 f0             	mov    -0x10(%ebp),%eax
 80c:	a3 8c 08 00 00       	mov    %eax,0x88c
      return (void*)(p + 1);
 811:	8b 45 ec             	mov    -0x14(%ebp),%eax
 814:	83 c0 08             	add    $0x8,%eax
 817:	eb 38                	jmp    851 <malloc+0xde>
    }
    if(p == freep)
 819:	a1 8c 08 00 00       	mov    0x88c,%eax
 81e:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 821:	75 1b                	jne    83e <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 823:	8b 45 f4             	mov    -0xc(%ebp),%eax
 826:	89 04 24             	mov    %eax,(%esp)
 829:	e8 ed fe ff ff       	call   71b <morecore>
 82e:	89 45 ec             	mov    %eax,-0x14(%ebp)
 831:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 835:	75 07                	jne    83e <malloc+0xcb>
        return 0;
 837:	b8 00 00 00 00       	mov    $0x0,%eax
 83c:	eb 13                	jmp    851 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 83e:	8b 45 ec             	mov    -0x14(%ebp),%eax
 841:	89 45 f0             	mov    %eax,-0x10(%ebp)
 844:	8b 45 ec             	mov    -0x14(%ebp),%eax
 847:	8b 00                	mov    (%eax),%eax
 849:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 84c:	e9 70 ff ff ff       	jmp    7c1 <malloc+0x4e>
}
 851:	c9                   	leave  
 852:	c3                   	ret    
